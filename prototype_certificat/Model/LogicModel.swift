//
//  LogicModel.swift
//  prototype_certificat
//
//  Created by Michelle Nguyen on 2022-01-29.
//

import Foundation

enum LogicModel: String {
    case Advance = "Advance"
    case Retreat = "Retreat"
    case For = "For"
    case If = "If"
}



