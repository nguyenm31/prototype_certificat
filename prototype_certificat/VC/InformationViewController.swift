//
//  InformationViewController.swift
//  prototype_certificat
//
//  Created by Michelle Nguyen on 2022-01-27.
//

import UIKit

class InformationViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    //Changement de ViewController
    @IBAction func next(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Test", bundle: nil)
        let secondVC = storyboard.instantiateViewController(identifier: "TestVC")

        secondVC.modalPresentationStyle = .fullScreen
        present(secondVC, animated: true, completion: nil)
    }
   
}
