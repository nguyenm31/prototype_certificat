//
//  TestViewController.swift
//  prototype_certificat
//
//  Created by Michelle Nguyen on 2022-01-28.
//

import UIKit

class TestViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
   
    var plane: Plane!
    @IBOutlet weak var planeIma: UIImageView!
    @IBOutlet weak var xPlane: NSLayoutConstraint!
    @IBOutlet weak var yPlane: NSLayoutConstraint!

    @IBOutlet weak var collectionView: UICollectionView!
    

    
    @IBOutlet weak var animationView: UIView!
    var logicElementArray = [LogicModel]()
    var press = false
    var elementHighlitgh = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        plane = Plane(x: 0,y: 0)
        xPlane.constant = 0
        
        // Implement the CollectionView Delegate
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    //Ajoute un élément avancé
    @IBAction func addAdvance(_ sender: Any) {
        if press {
            let indexPath = IndexPath(row: elementHighlitgh, section: 0)
            let cell = getForCell(indexPath: indexPath)
            cell.logicElementArray.append(LogicModel.Advance)
            cell.collectionView.reloadData()

        }else{
            logicElementArray.append(LogicModel.Advance)
            collectionView.reloadData()

        }
    }
    
    //Ajoute un élément de recule
    @IBAction func addRetreat(_ sender: Any) {
        if press {
            let indexPath = IndexPath(row: elementHighlitgh, section: 0)
            let cell = getForCell(indexPath: indexPath)
            cell.logicElementArray.append(LogicModel.Retreat)
            cell.collectionView.reloadData()

        }else{
            logicElementArray.append(LogicModel.Retreat)
            collectionView.reloadData()

        }

    }
    
    //Ajoute une boucle for
    @IBAction func addFor(_ sender: Any) {
        /*if press {
            let indexPath = IndexPath(row: elementHighlitgh, section: 0)
            let cell = getForCell(indexPath: indexPath)
            cell.logicElementArray.append(LogicModel.For)
            cell.collectionView.reloadData()

        }else{*/
            logicElementArray.append(LogicModel.For)
            collectionView.reloadData()

        //}

    }
    
    // Lecture des commandes
    @IBAction func play(_ sender: Any) {
        plane = Plane(x: 0,y: 0)
        Plane.play(logicElementArray: logicElementArray, collectionView: collectionView, plane: plane)
        xPlane.constant = CGFloat(plane.x)
       // yPlane.constant = CGFloat(plane.y)
    }
    
    
    ///Mark : - Coffiguration du CollectionView
    func getForCell(indexPath:IndexPath)->ForCollectionViewCell{
        let cell  = collectionView.cellForItem(at: indexPath) as! ForCollectionViewCell
        //cell.configureCell(dataAction: logicElementArray[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return logicElementArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //get a cell
        var cell: UICollectionViewCell? = nil
        
        
        switch logicElementArray[indexPath.row]{
        case .Advance:
             cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ActionCell", for: indexPath)
            let cellConf = cell as! ActionCollectionViewCell
            cellConf.configureCell(dataAction: logicElementArray[indexPath.row])
        case .Retreat:
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ActionCell", for: indexPath)
            let cellConf = cell as! ActionCollectionViewCell
            cellConf.configureCell(dataAction: logicElementArray[indexPath.row])

            break
        case .For:
             cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ForCell", for: indexPath)
            let cellConf = cell as! ForCollectionViewCell
            cellConf.configureCell(dataAction: logicElementArray[indexPath.row])

            break
        case .If:
            break
        }
        // return
        return cell!
    }

    //Souligne la bouble for
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cellArray = collectionView.visibleCells as! [ActionCollectionViewCell]
        
        if logicElementArray[indexPath.row] == LogicModel.For{
            let cell  = getForCell(indexPath: indexPath)

            if cell.press{
                cell.press = false
                cell.configureCell(dataAction: logicElementArray[indexPath.row])
                press = false
            }else {
                var index = 0
                for cellTemps in cellArray{
                    cellTemps.press = false
                    cellTemps.configureCell(dataAction: logicElementArray[index])
                    index += 1
                    
                }
                cell.press = true
                cell.configureCell(dataAction: logicElementArray[indexPath.row])
                press = true

            }
            elementHighlitgh = indexPath.row
        }
    }
    
   
    
}
