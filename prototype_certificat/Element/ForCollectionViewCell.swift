//
//  ForCollectionViewCell.swift
//  prototype_certificat
//
//  Created by Michelle Nguyen on 2022-01-29.
//

import UIKit

class ForCollectionViewCell: ActionCollectionViewCell, UICollectionViewDelegate, UICollectionViewDataSource {
    
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var timesField: UITextField!
    var times:Int = 2
    var logicElementArray = [LogicModel]()
    var firstTime = true;

    
    // CONFIGURATE THE CELL
    override func configureCell (dataAction:LogicModel){
        super.configureCell(dataAction: dataAction)
        if firstTime{
            // Implement the CollectionView Delegate
            collectionView.delegate = self
            collectionView.dataSource = self
        }
        if (press){
            background.layer.borderWidth = 5

        }else{
            background.layer.borderWidth = 3

        }
        
        if let text = timesField.text{
            if let number = Int(text){
                times = number
            }
        }
    }
    
    //Configuration du collectionView
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return logicElementArray.count

    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //get a cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ActionCell", for: indexPath) as! ActionCollectionViewCell
        
        // get a cell from an array
       // let courseCell: LogicModel = logicArray[indexPath.row]
        cell.configureCell(dataAction: logicElementArray[indexPath.row])
    
        // return it
        return cell
    }
    
    //Modification du nombre de répétitions
    @IBAction func numberChange(_ sender: Any) {
        if let text = timesField.text{
            if let number = Int(text){
                times = number
            }
        }
    }
}
