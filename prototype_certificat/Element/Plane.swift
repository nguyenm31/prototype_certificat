//
//  Plane.swift
//  prototype_certificat
//
//  Created by Michelle Nguyen on 2022-01-29.
//

import Foundation
import UIKit


class Plane{
    var x:Int
    var y:Int
    var angle:Float
    
    //Initialisation
    init(x:Int, y:Int){
        self.x = x
        self.y = y
        self.angle = 0
    }
    
    //Fonction pouvant déplacer l'élément
    func advance(){
        x += (Int)(15*cos(angle))
        y += (Int)(15*sin(angle))
    }
    
    func back(){
        x -= (Int)(15*cos(angle))
        y -= (Int)(15*sin(angle))
    }
    
    //Déplace l'élément selon les types de commandes choisies
    static func play (logicElementArray:[LogicModel], collectionView : UICollectionView, plane: Plane){
        if (((logicElementArray.count-1) >= 0)) {
            for step in 0...(logicElementArray.count-1){
                let element = logicElementArray[step]
                switch element {
                case .Advance:
                    plane.advance()
                    break
                case .Retreat:
                    plane.back()
                    break
                case .For:
                    let indexPath = IndexPath(row: step, section: 0)
                    let cell = collectionView.cellForItem(at: indexPath) as! ForCollectionViewCell
                    
                    for _ in 1...cell.times{
                        play(logicElementArray: cell.logicElementArray, collectionView : cell.collectionView, plane: plane)
                    }
                    break
                case .If:
                    break
                }
            }
        }
    }
}

