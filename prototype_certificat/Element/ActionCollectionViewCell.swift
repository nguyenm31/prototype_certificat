//
//  ActionCollectionViewCell.swift
//  prototype_certificat
//
//  Created by Michelle Nguyen on 2022-01-29.
//

import UIKit

class ActionCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var background: UIView!
    @IBOutlet weak var boxView: UIView!

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var deleteBtn: UIButton!
    var press = false
    
    // CONFIGURATE THE CELL
    func configureCell (dataAction:LogicModel){
    
        deleteBtn.isHidden = false
        // Set the color of the course
        background.backgroundColor = UIColor.white
        background.layer.borderWidth = 3
        background.layer.borderColor = UIColor.darkGray.cgColor
    
        // Affichage du titre
        titleLabel.text = dataAction.rawValue
    }

    // SHOW OR NOT THE DELETEBTN
    func configurationOfDeleteBtn(indexpath:IndexPath){
        deleteBtn.isHidden = !deleteBtn.isHidden
        deleteBtn.tag = indexpath.row
    }
}
